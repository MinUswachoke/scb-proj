package com.min.domainlayer.model

data class PhoneModel(
    val id: Int,
    val thumbImageURL: String,
    val brand: String,
    val name: String,
    val description: String,
    val price: Double,
    val rating: Double,
    var isFav: Boolean = false
)