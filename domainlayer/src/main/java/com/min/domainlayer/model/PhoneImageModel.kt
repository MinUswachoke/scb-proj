package com.min.domainlayer.model


data class PhoneImageModel(
    val imageUrl: String
)