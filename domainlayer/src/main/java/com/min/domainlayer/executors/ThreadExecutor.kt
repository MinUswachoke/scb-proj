package com.min.domainlayer.executors

import java.util.concurrent.Executor

interface ThreadExecutor : Executor
