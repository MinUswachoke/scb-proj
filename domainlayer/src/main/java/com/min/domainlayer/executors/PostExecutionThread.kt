package com.min.domainlayer.executors

import io.reactivex.Scheduler

interface PostExecutionThread{
    fun getScheduler(): Scheduler
}