package com.min.domainlayer.repositories

import com.min.domainlayer.model.PhoneModel
import io.reactivex.Observable

interface PhoneRepository{
    fun getPhoneList(): Observable<List<PhoneModel>>
    fun getPhoneFavList(): Observable<List<PhoneModel>>
    fun setFavorite(phoneModel: PhoneModel):Observable<Int>
    fun removeFavorite(phoneModel: PhoneModel):Observable<Int>
}