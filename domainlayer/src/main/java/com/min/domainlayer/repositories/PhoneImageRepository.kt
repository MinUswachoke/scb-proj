package com.min.domainlayer.repositories

import com.min.domainlayer.model.PhoneImageModel
import com.min.domainlayer.model.PhoneModel
import io.reactivex.Observable

interface PhoneImageRepository{
    fun getPhoneImage(id:Int): Observable<List<PhoneImageModel>>
}