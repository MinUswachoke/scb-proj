package com.min.domainlayer.usecase.phoneList

import com.min.domainlayer.executors.PostExecutionThread
import com.min.domainlayer.executors.ThreadExecutor
import com.min.domainlayer.model.PhoneModel
import com.min.domainlayer.repositories.PhoneRepository
import com.min.domainlayer.usecase.BaseUseCase
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

class GetPhoneListUseCase constructor(private val phoneRepository: PhoneRepository, threadExecutor: ThreadExecutor,
                                      postExecutionThread: PostExecutionThread): BaseUseCase<List<PhoneModel>, Void>(threadExecutor,postExecutionThread) {
    override fun getObservable(params: Void?): Observable<List<PhoneModel>> {
        return phoneRepository.getPhoneList()
    }
}