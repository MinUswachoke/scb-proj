package com.min.domainlayer.usecase.phoneImage

import com.min.domainlayer.executors.PostExecutionThread
import com.min.domainlayer.executors.ThreadExecutor
import com.min.domainlayer.model.PhoneImageModel
import com.min.domainlayer.model.PhoneModel
import com.min.domainlayer.repositories.PhoneImageRepository
import com.min.domainlayer.usecase.BaseUseCase
import io.reactivex.Observable

class GetPhoneImageUseCase constructor(private val phoneImageRepository: PhoneImageRepository, threadExecutor: ThreadExecutor,
                                       postExecutionThread: PostExecutionThread) : BaseUseCase<List<PhoneImageModel>, Int>(threadExecutor, postExecutionThread) {

    override fun getObservable(params: Int?): Observable<List<PhoneImageModel>> {
        return phoneImageRepository.getPhoneImage(params!!)
    }
}