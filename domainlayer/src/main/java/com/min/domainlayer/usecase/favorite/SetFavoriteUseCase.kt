package com.min.domainlayer.usecase.favorite

import com.min.domainlayer.executors.PostExecutionThread
import com.min.domainlayer.executors.ThreadExecutor
import com.min.domainlayer.model.PhoneModel
import com.min.domainlayer.repositories.PhoneRepository
import com.min.domainlayer.usecase.BaseUseCase
import io.reactivex.Observable

class SetFavoriteUseCase constructor(
        private val phoneRepository: PhoneRepository, threadExecutor: ThreadExecutor,
        postExecutionThread: PostExecutionThread) : BaseUseCase<Int, PhoneModel>(threadExecutor, postExecutionThread) {

    override fun getObservable(params: PhoneModel?): Observable<Int> {
        return phoneRepository.setFavorite(params!!)
    }
}