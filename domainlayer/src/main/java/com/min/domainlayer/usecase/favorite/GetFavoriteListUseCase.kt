package com.min.domainlayer.usecase.favorite

import com.min.domainlayer.executors.PostExecutionThread
import com.min.domainlayer.executors.ThreadExecutor
import com.min.domainlayer.model.PhoneModel
import com.min.domainlayer.repositories.PhoneRepository
import com.min.domainlayer.usecase.BaseUseCase
import io.reactivex.Observable

class GetFavoriteListUseCase constructor(private val phoneRepository: PhoneRepository, threadExecutor: ThreadExecutor,
                                      postExecutionThread: PostExecutionThread
): BaseUseCase<List<PhoneModel>, Void>(threadExecutor,postExecutionThread) {
    override fun getObservable(params: Void?): Observable<List<PhoneModel>> {
        return phoneRepository.getPhoneFavList()
    }
}