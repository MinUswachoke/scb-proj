package com.min.domainlayer.model.usecase

import com.min.domainlayer.executors.PostExecutionThread
import com.min.domainlayer.executors.ThreadExecutor
import com.min.domainlayer.model.PhoneModel
import com.min.domainlayer.repositories.PhoneRepository
import com.min.domainlayer.usecase.favorite.GetFavoriteListUseCase
import com.min.domainlayer.usecase.phoneList.GetPhoneListUseCase
import io.reactivex.Observable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class GetFavoriteListUseCaseTest {
    @Mock
    lateinit var threadExecutor: ThreadExecutor
    @Mock
    lateinit var postExecutionThread: PostExecutionThread
    @Mock
    lateinit var phoneDataRepository: PhoneRepository


    private lateinit var getFavoriteListUseCase: GetFavoriteListUseCase

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        getFavoriteListUseCase = GetFavoriteListUseCase(phoneDataRepository, threadExecutor, postExecutionThread)
        Mockito.`when`(postExecutionThread.getScheduler()).thenReturn(TestScheduler())
        var tempList = ArrayList<PhoneModel>()
        tempList.add(PhoneModel(1,"","","","",0.0,0.0,true))
        tempList.add(PhoneModel(2,"","","","",0.0,0.0,true))
        tempList.add(PhoneModel(3,"","","","",0.0,0.0,true))
        Mockito.`when`(phoneDataRepository.getPhoneFavList()).thenReturn(Observable.just(tempList))
    }

    @Test
    fun testGetList() {
        getFavoriteListUseCase.execute(object : DisposableObserver<List<PhoneModel>>(){
            override fun onNext(t: List<PhoneModel>) {
                Assert.assertEquals(t.size,3)
                Assert.assertEquals(t[0].isFav,true)
                Assert.assertEquals(t[1].isFav,true)
                Assert.assertEquals(t[2].isFav,true)

            }

            override fun onError(e: Throwable) {
            }

            override fun onComplete() {

            }

        }, null)

    }

}