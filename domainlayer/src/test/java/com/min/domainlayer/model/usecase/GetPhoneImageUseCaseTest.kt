package com.min.domainlayer.model.usecase

import com.min.domainlayer.executors.PostExecutionThread
import com.min.domainlayer.executors.ThreadExecutor
import com.min.domainlayer.model.PhoneImageModel
import com.min.domainlayer.model.PhoneModel
import com.min.domainlayer.repositories.PhoneImageRepository
import com.min.domainlayer.repositories.PhoneRepository
import com.min.domainlayer.usecase.phoneImage.GetPhoneImageUseCase
import com.min.domainlayer.usecase.phoneList.GetPhoneListUseCase
import io.reactivex.Observable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class GetPhoneImageUseCaseTest {
    @Mock
    lateinit var threadExecutor: ThreadExecutor
    @Mock
    lateinit var postExecutionThread: PostExecutionThread
    @Mock
    lateinit var phoneDataRepository: PhoneImageRepository


    private lateinit var getPhoneListUseCase: GetPhoneImageUseCase

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        getPhoneListUseCase = GetPhoneImageUseCase(phoneDataRepository, threadExecutor, postExecutionThread)
        Mockito.`when`(postExecutionThread.getScheduler()).thenReturn(TestScheduler())
        var tempList = ArrayList<PhoneImageModel>()
        tempList.add(PhoneImageModel("url"))
        tempList.add(PhoneImageModel("link"))
        Mockito.`when`(phoneDataRepository.getPhoneImage(1)).thenReturn(Observable.just(tempList))
    }

    @Test
    fun testGetList() {
        getPhoneListUseCase.execute(object : DisposableObserver<List<PhoneImageModel>>(){
            override fun onNext(t: List<PhoneImageModel>) {
                Assert.assertEquals(t.size,2)
                Assert.assertEquals(t[0].imageUrl,"url")
                Assert.assertEquals(t[0].imageUrl,"link")
            }

            override fun onError(e: Throwable) {

            }

            override fun onComplete() {

            }

        }, null)

    }

}