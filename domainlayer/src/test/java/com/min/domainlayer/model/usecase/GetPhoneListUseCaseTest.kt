package com.min.domainlayer.model.usecase

import android.util.Log
import com.min.domainlayer.executors.PostExecutionThread
import com.min.domainlayer.executors.ThreadExecutor
import com.min.domainlayer.model.PhoneModel
import com.min.domainlayer.repositories.PhoneRepository
import com.min.domainlayer.usecase.favorite.GetFavoriteListUseCase
import com.min.domainlayer.usecase.favorite.SetFavoriteUseCase
import com.min.domainlayer.usecase.phoneList.GetPhoneListUseCase
import io.reactivex.Observable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class GetPhoneListUseCaseTest {
    @Mock
    lateinit var threadExecutor: ThreadExecutor
    @Mock
    lateinit var postExecutionThread: PostExecutionThread
    @Mock
    lateinit var phoneDataRepository: PhoneRepository


    private lateinit var getPhoneListUseCase: GetPhoneListUseCase

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        getPhoneListUseCase = GetPhoneListUseCase(phoneDataRepository, threadExecutor, postExecutionThread)
        Mockito.`when`(postExecutionThread.getScheduler()).thenReturn(TestScheduler())
        var tempList = ArrayList<PhoneModel>()
        tempList.add(PhoneModel(1,"","","","",0.0,0.0,true))
        Mockito.`when`(phoneDataRepository.getPhoneList()).thenReturn(Observable.just(tempList))
    }

    @Test
    fun testGetList() {
        getPhoneListUseCase.execute(object : DisposableObserver<List<PhoneModel>>(){
            override fun onNext(t: List<PhoneModel>) {
                Assert.assertEquals(t.size,1)
                Assert.assertEquals(t[0].id,1)
            }

            override fun onError(e: Throwable) {
            }

            override fun onComplete() {

            }

        }, null)

    }

}