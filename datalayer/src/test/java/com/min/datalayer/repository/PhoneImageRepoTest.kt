package com.min.datalayer.repository

import com.min.datalayer.dataRepositories.PhoneImageDataRepository
import com.min.datalayer.mappers.PhoneImageMapper
import com.min.datalayer.model.api.PhoneImageApi
import com.min.datalayer.network.RetrofitInterface
import io.reactivex.Observable
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.Spy
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class PhoneImageRepoTest {
    @Mock
    lateinit var retrofitInterface: RetrofitInterface
    @Spy
    var phoneEntityDataMapper = PhoneImageMapper()

    lateinit var repository: PhoneImageDataRepository

    val phoneApiReturn = ArrayList<PhoneImageApi>()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        repository = PhoneImageDataRepository(retrofitInterface, phoneEntityDataMapper)

        phoneApiReturn.add(PhoneImageApi(1, "1", "image1"))
        phoneApiReturn.add(PhoneImageApi(2, "2", "image2"))
        phoneApiReturn.add(PhoneImageApi(3, "3", "image3"))
        Mockito.`when`(retrofitInterface.getMobileImage(1)).thenReturn(Observable.just(phoneApiReturn))
    }

    @Test
    fun testGetPhoneImage() {
        var result = repository.getPhoneImage(1)
        Mockito.verify(retrofitInterface, Mockito.times(1)).getMobileImage(1)
        Assert.assertEquals(result.blockingFirst()[0].imageUrl, phoneApiReturn[0].imageUrl)
        Assert.assertEquals(result.blockingFirst()[1].imageUrl, phoneApiReturn[1].imageUrl)
        Assert.assertEquals(result.blockingFirst()[2].imageUrl, phoneApiReturn[2].imageUrl)
    }


    private fun <T> any(): T {
        Mockito.any<T>()
        return uninitialized()
    }

    private fun <T> uninitialized(): T = null as T

}