package com.min.datalayer.repository

import com.min.datalayer.dao.PhoneDao
import com.min.datalayer.dataRepositories.PhoneDataRepository
import com.min.datalayer.mappers.PhoneEntityDataMapper
import com.min.datalayer.model.api.PhoneDataApi
import com.min.datalayer.model.db.PhoneDb
import com.min.datalayer.network.RetrofitInterface
import com.min.domainlayer.model.PhoneModel
import io.reactivex.Observable
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PhoneRepositortTest {
    @Mock
    lateinit var phoneDao: PhoneDao
    @Mock
    lateinit var retrofitInterface: RetrofitInterface
    @Spy
    var phoneEntityDataMapper = PhoneEntityDataMapper()

    lateinit var repository: PhoneDataRepository


    val phoneDaoReturn = ArrayList<PhoneDb>()
    val phoneApiReturn = ArrayList<PhoneDataApi>()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        repository = PhoneDataRepository(retrofitInterface, phoneEntityDataMapper, phoneDao)
        phoneDaoReturn.add(PhoneDb(1,"testDao","best phone",10.0,11.0,"no image",true))
        Mockito.`when`(phoneDao.getFavList(true)).thenReturn(phoneDaoReturn)

        phoneApiReturn.add(PhoneDataApi(1,"no image","brand","testDao","best phone",10.0,11.0))
        phoneApiReturn.add(PhoneDataApi(2,"no image2","brand2","testDao2","best phone2",10.01,11.01))
        phoneApiReturn.add(PhoneDataApi(3,"no image3","brand3","testDao3","best phone3",10.02,11.02))
        Mockito.`when`(retrofitInterface.getMobileList()).thenReturn(Observable.just(phoneApiReturn))

        Mockito.`when`(phoneDao.updateFavorite(any())).thenReturn(1)
    }

    @Test
    fun testGetPhoneList(){
//
        var result = repository.getPhoneList()
        Mockito.verify(phoneDao,Mockito.times(1)).getFavList(true)
        Mockito.verify(retrofitInterface,Mockito.times(1)).getMobileList()

        Assert.assertEquals(result.blockingFirst()[0].id,1)
        Assert.assertEquals(result.blockingFirst()[1].id,2)
        Assert.assertEquals(result.blockingFirst()[2].id,3)
        Assert.assertEquals(result.blockingFirst()[0].isFav,true)
        Assert.assertEquals(result.blockingFirst()[1].isFav,false)
        Assert.assertEquals(result.blockingFirst()[2].isFav,false)
    }




    private fun <T> any(): T {
        Mockito.any<T>()
        return uninitialized()
    }

    private fun <T> uninitialized(): T = null as T

}