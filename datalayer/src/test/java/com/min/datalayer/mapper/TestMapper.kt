package com.min.datalayer.mapper

import com.min.datalayer.mappers.PhoneEntityDataMapper
import com.min.datalayer.model.api.PhoneDataApi
import com.min.datalayer.model.db.PhoneDb
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class TestMapper{
    var mapper = PhoneEntityDataMapper()

    @Test
    fun TestTransformApiToData(){
        var itemApi = PhoneDataApi(1,"image_url","brand","samsung","test long desc",
            15.0,4.0)
        var item = mapper.transformApiToData(itemApi)
        Assert.assertEquals(itemApi.id,item.id)
        Assert.assertEquals(itemApi.thumbImageURL,item.thumbImageURL)
        Assert.assertEquals(itemApi.brand,item.brand)
        Assert.assertEquals(itemApi.name,item.name)
        Assert.assertEquals(itemApi.description,item.description)
        Assert.assertEquals(itemApi.price,item.price,15.0)
        Assert.assertEquals(itemApi.rating,item.rating,4.0)
    }

    @Test
    fun TestTransformDbToData(){
        var itemApi = PhoneDb(1,"Samsung A9","the latest phone",18.0,1.0,"image",true)
        var item = mapper.transformDbToData(itemApi)
        Assert.assertEquals(itemApi.id,item.id)
        Assert.assertEquals(itemApi.name,item.name)
        Assert.assertEquals(itemApi.description,item.description)
        Assert.assertEquals(itemApi.imageUrl,item.thumbImageURL)
        Assert.assertEquals(itemApi.price,item.price,15.0)
        Assert.assertEquals(itemApi.rate,item.rating,4.0)
        Assert.assertEquals(itemApi.isFavorite,item.isFav)
    }
}