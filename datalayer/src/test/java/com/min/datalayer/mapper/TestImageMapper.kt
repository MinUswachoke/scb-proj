package com.min.datalayer.mapper

import com.min.datalayer.mappers.PhoneImageMapper
import com.min.datalayer.model.api.PhoneImageApi
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Spy
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class TestImageMapper {
    @Spy
    val mapper = PhoneImageMapper()


    @Test
    fun TestTransformApiToData() {

        val itemList = ArrayList<PhoneImageApi>()
        itemList.add(PhoneImageApi(1, "1", "image1"))
        itemList.add(PhoneImageApi(2, "2", "image2"))
        itemList.add(PhoneImageApi(3, "3", "image3"))
        itemList.add(PhoneImageApi(4, "4", "image4"))
        var item = mapper.transformApiToDataList(itemList)
        Mockito.verify(mapper, Mockito.times(4)).transformApiToData(any())
        Mockito.verify(mapper).transformApiToData(itemList[0])
        Mockito.verify(mapper).transformApiToData(itemList[1])
        Mockito.verify(mapper).transformApiToData(itemList[2])
        Mockito.verify(mapper).transformApiToData(itemList[3])
        Assert.assertEquals(item.size, 4)

    }

    private fun <T> any(): T {
        Mockito.any<T>()
        return uninitialized()
    }

    private fun <T> uninitialized(): T = null as T
}