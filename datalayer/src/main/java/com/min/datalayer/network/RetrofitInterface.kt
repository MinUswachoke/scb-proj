package com.min.datalayer.network

import com.min.datalayer.model.api.PhoneDataApi
import com.min.datalayer.model.api.PhoneImageApi
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path


interface RetrofitInterface{
    @GET("mobiles")
    fun getMobileList(): Observable<List<PhoneDataApi>>

    @GET("mobiles/{mobile_id}/images/")
    fun getMobileImage(@Path("mobile_id") id:Int): Observable<List<PhoneImageApi>>
}