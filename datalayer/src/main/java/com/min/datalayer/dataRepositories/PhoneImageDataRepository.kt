package com.min.datalayer.dataRepositories

import com.min.datalayer.mappers.PhoneImageMapper
import com.min.datalayer.network.RetrofitInterface
import com.min.domainlayer.model.PhoneImageModel
import com.min.domainlayer.repositories.PhoneImageRepository
import io.reactivex.Observable

class PhoneImageDataRepository constructor(
    private val retrofitInterface: RetrofitInterface,
    private val phoneEntityDataMapper: PhoneImageMapper
) : PhoneImageRepository {
    override fun getPhoneImage(id: Int): io.reactivex.Observable<List<PhoneImageModel>> {
        return retrofitInterface.getMobileImage(id).concatMap { r ->
            return@concatMap Observable.just(phoneEntityDataMapper.transformApiToDataList(r))
        }
    }

}