package com.min.datalayer.dataRepositories

import android.annotation.SuppressLint
import android.content.ContentValues.TAG
import android.util.Log
import com.min.datalayer.dao.PhoneDao
import com.min.datalayer.mappers.PhoneEntityDataMapper
import com.min.datalayer.model.db.PhoneDb
import com.min.datalayer.network.RetrofitInterface
import com.min.domainlayer.model.PhoneModel
import com.min.domainlayer.repositories.PhoneRepository
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers

class PhoneDataRepository constructor(
    private val retrofitInterface: RetrofitInterface,
    private val phoneEntityDataMapper: PhoneEntityDataMapper,
    private val phoneDao: PhoneDao) : PhoneRepository {

    override fun getPhoneList(): io.reactivex.Observable<List<PhoneModel>> {
        val dbObs = Observable.just(phoneDao.getFavList(true))
        val apiObs = retrofitInterface.getMobileList().concatMap { r ->
            Observable.just(phoneEntityDataMapper.transformApiToDataList(r))
        }
        return Observable.zip(apiObs, dbObs, BiFunction { t1, t2 ->
            val tempList = ArrayList<PhoneModel>()
            for (i in 0 until t1.size) {
                var contain = false
                for (j in 0 until t2.size) {
                    if (t1[i].id == t2[j].id) {
                        contain = true
                    }
                }
                val item = t1[i]
                item.isFav = contain
                tempList.add(item)
            }
            return@BiFunction tempList
        })
    }

    override fun getPhoneFavList(): Observable<List<PhoneModel>> {
        return Observable.just(phoneDao.getFavList(true)).concatMap { r ->
            Observable.just(phoneEntityDataMapper.transformDbToDataList(r))
        }
    }


    @SuppressLint("CheckResult")
    override fun setFavorite(phoneModel: PhoneModel): Observable<Int> {
        val itemDb = PhoneDb(
            phoneModel.id,
            phoneModel.name,
            phoneModel.description,
            phoneModel.price,
            phoneModel.rating,
            phoneModel.thumbImageURL,
            true
        )
        Observable.just(phoneDao.updateFavorite(itemDb))
        return Observable.just(0)
    }

    @SuppressLint("CheckResult")
    override fun removeFavorite(phoneModel: PhoneModel): Observable<Int> {
        val itemDb = PhoneDb(
            phoneModel.id,
            phoneModel.name,
            phoneModel.description,
            phoneModel.price,
            phoneModel.rating,
            phoneModel.thumbImageURL,
            true
        )
        Observable.just(phoneDao.deleteFromFavorite(itemDb))
            .subscribeOn(Schedulers.io())
            .subscribe { db -> Log.e(TAG, db.toString()) }
        return Observable.just(0)
    }



}