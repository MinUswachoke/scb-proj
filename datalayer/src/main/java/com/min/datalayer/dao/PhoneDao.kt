package com.min.datalayer.dao

import android.arch.persistence.room.*
import com.min.datalayer.model.db.PhoneDb


@Dao
interface PhoneDao {
    @Query("SELECT * from phones WHERE isFavorite=:isFav")
    fun getFavList(isFav: Boolean): List<PhoneDb>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateFavorite(phoneDb: PhoneDb): Long

    @Delete
    fun deleteFromFavorite(phoneDb: PhoneDb)
}