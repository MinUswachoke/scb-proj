package com.min.datalayer.executors

import com.min.domainlayer.executors.ThreadExecutor
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadFactory
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit


class JobExecutor: ThreadExecutor {
    private val threadPoolExecutor = ThreadPoolExecutor(3,5,10,
        TimeUnit.SECONDS,LinkedBlockingQueue(),JobThreadFactory())


    override fun execute(p0: Runnable?) {
        this.threadPoolExecutor.execute(p0)
    }

    private class JobThreadFactory : ThreadFactory {
        private var counter = 0

        override fun newThread(runnable: Runnable): Thread {
            return Thread(runnable, "android_" + counter++)
        }
    }
}