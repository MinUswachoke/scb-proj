package com.min.datalayer.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.min.datalayer.dao.PhoneDao
import com.min.datalayer.model.db.PhoneDb


@Database(entities = [PhoneDb::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun phoneDao(): PhoneDao
}