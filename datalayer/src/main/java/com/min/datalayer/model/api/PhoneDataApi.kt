package com.min.datalayer.model.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PhoneDataApi(
    @SerializedName("id") @Expose val id: Int,
    @SerializedName("thumbImageURL") @Expose val thumbImageURL: String,
    @SerializedName("brand") @Expose val brand: String,
    @SerializedName("name") @Expose val name: String,
    @SerializedName("description") @Expose val description: String,
    @SerializedName("price") @Expose val price: Double,
    @SerializedName("rating") @Expose val rating: Double
)