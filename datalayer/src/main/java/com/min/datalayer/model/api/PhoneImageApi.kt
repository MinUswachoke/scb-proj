package com.min.datalayer.model.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PhoneImageApi(
    @SerializedName("id") @Expose val id: Int,
    @SerializedName("mobile_id") @Expose val mobileId: String,
    @SerializedName("url") @Expose val imageUrl: String
)