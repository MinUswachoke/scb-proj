package com.min.datalayer.model.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "phones")
data class PhoneDb(@PrimaryKey(autoGenerate = true) var id: Int?,
                       @ColumnInfo(name = "name") var name: String,
                       @ColumnInfo(name = "description") var description: String,
                       @ColumnInfo(name = "price") var price: Double,
                       @ColumnInfo(name = "rate") var rate: Double,
                       @ColumnInfo(name = "imageUrl") var imageUrl: String,
                       @ColumnInfo(name = "isFavorite") var isFavorite: Boolean = false
){
    constructor():this(0,"","",0.0,0.0,"", false)
}