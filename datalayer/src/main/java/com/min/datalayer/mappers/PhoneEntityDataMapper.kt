package com.min.datalayer.mappers

import com.min.datalayer.model.api.PhoneDataApi
import com.min.datalayer.model.db.PhoneDb
import com.min.domainlayer.model.PhoneModel

class PhoneEntityDataMapper {
    fun transformApiToDataList(dataEntities: List<PhoneDataApi>): List<PhoneModel> {
        val itemArray = ArrayList<PhoneModel>()
        dataEntities.mapTo(itemArray) { transformApiToData(it) }
        return itemArray
    }

    fun transformApiToData(data: PhoneDataApi): PhoneModel {
       return PhoneModel(data.id,data.thumbImageURL,data.brand,data.name,data.description,
           data.price,data.rating)
    }

    fun transformDbToDataList(dataEntities: List<PhoneDb>): List<PhoneModel> {
        val itemArray = ArrayList<PhoneModel>()
        dataEntities.mapTo(itemArray) { transformDbToData(it) }
        return itemArray
    }

    fun transformDbToData(data: PhoneDb): PhoneModel {
        return PhoneModel(data.id!!,data.imageUrl,"",data.name,data.description,
            data.price,data.rate,data.isFavorite)
    }
}