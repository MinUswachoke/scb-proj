package com.min.datalayer.mappers

import com.min.datalayer.model.api.PhoneImageApi
import com.min.domainlayer.model.PhoneImageModel

class PhoneImageMapper {

    fun transformApiToDataList(dataEntities: List<PhoneImageApi>): List<PhoneImageModel> {
        val itemArray = ArrayList<PhoneImageModel>()
        dataEntities.mapTo(itemArray) { transformApiToData(it) }
        return itemArray
    }

    fun transformApiToData(data: PhoneImageApi): PhoneImageModel {
        return PhoneImageModel(data.imageUrl)
    }
}