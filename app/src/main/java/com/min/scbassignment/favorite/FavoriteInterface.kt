package com.min.scbassignment.favorite

import com.min.scbassignment.base.view.BaseFragmentInterface
import com.min.scbassignment.enums.SortType
import com.min.scbassignment.mainpage.MainActivityInterface

interface FavoriteInterface:BaseFragmentInterface{
    fun setInterface(mainActivityInterface: MainActivityInterface)
}