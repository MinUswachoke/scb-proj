package com.min.scbassignment.favorite

import android.arch.lifecycle.Observer
import android.databinding.Observable
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View
import com.min.domainlayer.model.PhoneModel
import com.min.scbassignment.R
import com.min.scbassignment.adapters.FavoriteAdapter
import com.min.scbassignment.adapters.SwipeRemoveCallBack
import com.min.scbassignment.adapters.interfaces.FavoriteOnSwipe
import com.min.scbassignment.base.view.BaseActivityInterface
import com.min.scbassignment.base.view.BaseFragment
import com.min.scbassignment.base.viewModel.BaseFragmentViewMode
import com.min.scbassignment.databinding.FragmentFavoriteBinding
import com.min.scbassignment.mainpage.MainActivityInterface
import javax.inject.Inject

class FavoriteFragment : BaseFragment(), FavoriteInterface {

    @Inject
    lateinit var adapter: FavoriteAdapter
    @Inject
    lateinit var viewModel: FavoriteViewModel

    private lateinit var mViewBinding: FragmentFavoriteBinding

    private lateinit var activityInterface: MainActivityInterface

    override fun setInterface(mainActivityInterface: MainActivityInterface) {
        activityInterface = mainActivityInterface
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewBinding = getViewBinding(FragmentFavoriteBinding::class.java)
        initializeView()
    }

    override fun initViewModel() {
        super.initViewModel()
        viewModel.phoneList.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                viewModel.phoneList.get()?.let {
                    adapter.setItems(it)
                }
            }

        })

        viewModel.reloadList.observe(this, Observer {
            viewModel.phoneList.get()?.let {
                adapter.setItems(it)
            }
        })

        viewModel.removeFavoriteMainList.observe(this, Observer {
            it?.let {
                activityInterface.removeFavoriteMain(it)
            }
        })
    }

    override fun getBaseViewModel(): BaseFragmentViewMode {
        return viewModel
    }

    override fun getLayoutResId(): Int {
        return R.layout.fragment_favorite
    }

    override fun showError(eror: String) {
        (activityInterface as BaseActivityInterface).showErrorMsg(eror)
    }

    private fun initializeView() {
        mViewBinding.phoneRecyclerView
        mViewBinding.phoneRecyclerView.layoutManager = LinearLayoutManager(activity)
        mViewBinding.phoneRecyclerView.adapter = adapter
        adapter.setInterface(object : FavoriteOnSwipe {
            override fun onSwipe(phoneModel: PhoneModel) {

            }

            override fun onClick(phoneModel: PhoneModel) {
                activityInterface.showDetailPage(phoneModel)
            }

        })

        val swipeHandler = object : SwipeRemoveCallBack(activity!!.baseContext!!) {
            override fun onSwiped(p0: RecyclerView.ViewHolder, p1: Int) {
                viewModel.phoneList.get()?.let {
                    viewModel.removeFromFav(it[p0.adapterPosition], p0.adapterPosition)
                }

            }

            override fun onMove(p0: RecyclerView, p1: RecyclerView.ViewHolder, p2: RecyclerView.ViewHolder): Boolean {
                return true
            }
        }

        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(mViewBinding.phoneRecyclerView)
    }
}