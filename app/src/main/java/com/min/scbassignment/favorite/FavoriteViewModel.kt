package com.min.scbassignment.favorite

import android.app.Application
import android.util.Log
import com.min.domainlayer.model.PhoneModel
import com.min.domainlayer.usecase.favorite.RemoveFavoriteUseCase
import com.min.scbassignment.base.viewModel.BaseFragmentViewMode
import com.min.scbassignment.events.SingleLiveEvent
import io.reactivex.observers.DisposableObserver


class FavoriteViewModel(val app: Application, private val removeFavoriteUseCase: RemoveFavoriteUseCase) :
    BaseFragmentViewMode(app) {
    private val TAG = "MainViewModel"

    val removeFavoriteMainList = SingleLiveEvent<Int>()

    fun removeFromFav(phoneModel: PhoneModel, index: Int) {
        removeFavoriteUseCase.execute(object : DisposableObserver<Int>() {
            override fun onComplete() {
                removeIndex(index)
                removeFavoriteMainList.value = phoneModel.id
            }

            override fun onNext(t: Int) {

            }

            override fun onError(e: Throwable) {
                errorMsg.value = e.localizedMessage

            }

        }, phoneModel)
    }

    fun removeIndex(index: Int) {
        phoneList.get()?.let {
            val tempList = ArrayList<PhoneModel>()
            tempList.addAll(it)
            tempList.removeAt(index)
            setData(tempList)
            reloadList.call()
        }
    }

}