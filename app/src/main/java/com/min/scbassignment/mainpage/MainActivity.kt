package com.min.scbassignment.mainpage

import android.arch.lifecycle.Observer
import android.content.DialogInterface
import android.databinding.Observable
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.FrameLayout
import android.widget.ProgressBar
import com.min.domainlayer.model.PhoneModel
import com.min.scbassignment.R
import com.min.scbassignment.adapters.MainViewAdapter
import com.min.scbassignment.base.view.BaseActivity
import com.min.scbassignment.base.viewModel.BaseActivityViewModel
import com.min.scbassignment.databinding.ActivityMainBinding
import com.min.scbassignment.detailPage.DetailActivity
import com.min.scbassignment.enums.SortType
import com.min.scbassignment.favorite.FavoriteFragment
import com.min.scbassignment.listpage.ListPageFragment
import javax.inject.Inject

class MainActivity : BaseActivity(), MainActivityInterface {
    @Inject
    lateinit var viewModel: MainViewModel
    @Inject
    lateinit var mainViewAdapter: MainViewAdapter

    private lateinit var mViewBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewBinding = getViewBinding(ActivityMainBinding::class.java)
        mViewBinding.viewModel = viewModel
        viewModel.getData()
        initViewPager()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        viewModel.showSortDialog()
        return true
    }


    override fun initViewModel() {
        super.initViewModel()
        viewModel.phoneList.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                viewModel.phoneList.get()?.let {
                    mainViewAdapter.getListPage().populateList(it)
                }
            }

        })

        viewModel.favoriteList.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                viewModel.favoriteList.get()?.let {
                    mainViewAdapter.getFavoritePage().populateList(it)
                }
            }

        })

        viewModel.showSortDialog.observe(this, Observer { defaultSort ->
            defaultSort?.let {
                showSortDialog(it)
            }

        })

        viewModel.sortType.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                viewModel.sortType.get()?.let {
                    mainViewAdapter.getListPage().sort(it)
                    mainViewAdapter.getFavoritePage().sort(it)
                }
            }

        })
    }

    private fun showSortDialog(sortType: SortType) {
        lateinit var dialog: AlertDialog

        val array = arrayOf(
            getString(R.string.label_sort_rating),
            getString(R.string.label_sort_price_low_to_high),
            getString(R.string.label_sort_price_high_to_low)
        )
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.label_pick_sort)
            .setSingleChoiceItems(array, sortType.ordinal) { _, p1 ->
                viewModel.sortType.set(SortType.values()[p1])
                dialog.dismiss()
            }

        dialog = builder.create()
        dialog.show()
    }

    private fun initViewPager() {
        mainViewAdapter.init(this, supportFragmentManager, ListPageFragment(), FavoriteFragment())
        mViewBinding.mainViewPager.adapter = mainViewAdapter.fragmentStateAdapter
        mViewBinding.mainTabLayout.setupWithViewPager(mViewBinding.mainViewPager)

    }

    override fun getBaseViewModel(): BaseActivityViewModel {
        return viewModel
    }

    override fun getLayoutResId(): Int {
        return R.layout.activity_main
    }

    override fun getLoadingProgressBar(): ProgressBar {
        return mViewBinding.progressbar
    }


    override fun getFragmentContainer(): FrameLayout? {
        return null
    }

    override fun getRootView(): View {
        return mViewBinding.root
    }

    override fun showDetailPage(phoneModel: PhoneModel) {
        DetailActivity.getInstance(this, phoneModel)
    }

    override fun favoriteReload() {
        viewModel.loadFavoriteList()
    }

    override fun removeFavoriteMain(id: Int) {
        mainViewAdapter.getListPage().removeFavoriteIndex(id)
    }

}