package com.min.scbassignment.mainpage

import android.app.Application
import android.databinding.ObservableField
import android.util.Log
import com.min.domainlayer.model.PhoneModel
import com.min.domainlayer.usecase.favorite.GetFavoriteListUseCase
import com.min.domainlayer.usecase.phoneList.GetPhoneListUseCase
import com.min.scbassignment.base.viewModel.BaseActivityViewModel
import com.min.scbassignment.enums.SortType
import com.min.scbassignment.events.SingleLiveEvent
import io.reactivex.observers.DisposableObserver

class MainViewModel constructor(val app: Application, private val getPhoneListUseCase: GetPhoneListUseCase,
                                private val getFavoriteListUseCase: GetFavoriteListUseCase) : BaseActivityViewModel(app) {
    private val TAG = "MainViewModel"

    val phoneList = ObservableField<List<PhoneModel>>()
    val favoriteList = ObservableField<List<PhoneModel>>()
    val sortType = ObservableField<SortType>(SortType.SORT_RATING_HIGH_LOW)
    val showSortDialog = SingleLiveEvent<SortType>()


    fun getData() {
        isLoading.set(true)
        getPhoneListUseCase.execute(object : DisposableObserver<List<PhoneModel>>() {
            override fun onComplete() {
                isLoading.set(false)
            }

            override fun onNext(t: List<PhoneModel>) {
                phoneList.set(t)
                getFavoriteList(t)
            }

            override fun onError(e: Throwable) {
                errorMsg.value = e.localizedMessage
                isLoading.set(false)
            }

        }, null)
    }

    fun showSortDialog(){
        showSortDialog.value = sortType.get()
    }

    fun loadFavoriteList(){
        isLoading.set(true)
        getFavoriteListUseCase.execute(object : DisposableObserver<List<PhoneModel>>() {
            override fun onComplete() {
                isLoading.set(false)
            }

            override fun onNext(t: List<PhoneModel>) {
                favoriteList.set(t)
            }

            override fun onError(e: Throwable) {
                Log.e(TAG, e.localizedMessage)
                isLoading.set(false)
            }

        }, null)
    }

    fun getFavoriteList(phoneModels: List<PhoneModel>){
        val tempList = ArrayList<PhoneModel>()
        for(i in 0 until phoneModels.size){
            if(phoneModels[i].isFav){
                tempList.add(phoneModels[i])
            }
        }
        favoriteList.set(tempList)
    }
}