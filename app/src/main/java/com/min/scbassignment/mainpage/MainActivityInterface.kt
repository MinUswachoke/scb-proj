package com.min.scbassignment.mainpage

import com.min.domainlayer.model.PhoneModel
import com.min.scbassignment.base.view.BaseActivityInterface

interface MainActivityInterface: BaseActivityInterface {
    fun showDetailPage(phoneModel: PhoneModel)
    fun favoriteReload()

    /**
     * clear favorite from main list
     */
    fun removeFavoriteMain(id: Int)
}