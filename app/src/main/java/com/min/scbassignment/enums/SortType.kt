package com.min.scbassignment.enums

enum class SortType {
    SORT_RATING_HIGH_LOW,
    SORT_PRICE_LOW_TO_HIGH,
    SORT_PRICE_HIGH_TO_LOW
}