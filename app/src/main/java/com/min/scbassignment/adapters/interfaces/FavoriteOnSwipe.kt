package com.min.scbassignment.adapters.interfaces

import com.min.domainlayer.model.PhoneModel

interface FavoriteOnSwipe{
    fun onSwipe(phoneModel: PhoneModel)
    fun onClick(phoneModel: PhoneModel)
}