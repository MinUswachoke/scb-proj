package com.min.scbassignment.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.min.scbassignment.R
import com.min.scbassignment.favorite.FavoriteFragment
import com.min.scbassignment.favorite.FavoriteInterface
import com.min.scbassignment.listpage.ListPageFragment
import com.min.scbassignment.listpage.ListPageInterface
import com.min.scbassignment.mainpage.MainActivity

class MainViewAdapter {
    lateinit var fragmentStateAdapter: FragmentStatePagerAdapter

    fun init(
        context: MainActivity,
        fragmentManager: FragmentManager,
        listPageFragment: ListPageFragment,
        favoriteFragment: FavoriteFragment
    ) {
        listPageFragment.setInterface(context)
        favoriteFragment.setInterface(context)
        fragmentStateAdapter = object : FragmentStatePagerAdapter(fragmentManager) {
            override fun getItem(position: Int): Fragment {
                return when (position) {
                    0 -> listPageFragment
                    1 -> favoriteFragment
                    else -> listPageFragment
                }
            }

            override fun getPageTitle(position: Int): CharSequence? {

                return when (position) {
                    0 -> context.getString(R.string.label_mobile_list)
                    1 -> context.getString(R.string.label_fav_list)
                    else -> context.getString(R.string.label_mobile_list)
                }

            }

            override fun getCount(): Int {
                return 2
            }
        }
    }

    fun getListPage(): ListPageInterface {
        return fragmentStateAdapter.getItem(0) as ListPageInterface
    }

    fun getFavoritePage(): FavoriteInterface {
        return fragmentStateAdapter.getItem(1) as FavoriteInterface
    }
}