package com.min.scbassignment.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.min.domainlayer.model.PhoneModel
import com.min.scbassignment.R
import com.min.scbassignment.adapters.interfaces.MainListOnClick
import com.min.scbassignment.databinding.ItemPhoneBinding
import com.squareup.picasso.Picasso

class PhoneListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var itemList: ArrayList<PhoneModel> = ArrayList()
    private lateinit var clickInterface: MainListOnClick

    fun setInterface(clickInterface: MainListOnClick) {
        this.clickInterface = clickInterface
    }

    fun setItems(items: List<PhoneModel>) {
        itemList.clear()
        itemList.addAll(items)
        notifyDataSetChanged()
    }


    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        var context = viewHolder.itemView.context
        val holder = viewHolder as ItemHolder
        var item = itemList.get(position)
        holder.bind()
        holder.itemBinding.nameTextView.text = item.name
        holder.itemBinding.descTextView.text = item.description
        holder.itemBinding.priceTextView.text = context.getString(R.string.label_price, item.price.toString())
        holder.itemBinding.ratingTextView.text = context.getString(R.string.label_rating, item.rating.toString())
        Picasso.get().load(item.thumbImageURL).into(holder.itemBinding.phoneImageView)
        Picasso.get().load(if(item.isFav)R.drawable.ic_heart_filled else R.drawable.ic_heart).into(holder.itemBinding.favoriteImageView)

        holder.itemBinding.root.setOnClickListener {
            clickInterface.onClick(item)
        }
        holder.itemBinding.favoriteImageView.setOnClickListener {
            clickInterface.onClickFav(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemBinding = ItemPhoneBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        )
        return ItemHolder(itemBinding)
    }

    private inner class ItemHolder internal constructor(var itemBinding: ItemPhoneBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind() {
            itemBinding.executePendingBindings()
        }
    }
}