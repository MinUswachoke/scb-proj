package com.min.scbassignment.adapters.interfaces

import com.min.domainlayer.model.PhoneModel

interface MainListOnClick{
    fun onClick(phoneModel: PhoneModel)
    fun onClickFav(phoneModel: PhoneModel)
}