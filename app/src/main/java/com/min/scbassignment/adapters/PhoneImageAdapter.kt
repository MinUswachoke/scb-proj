package com.min.scbassignment.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.min.domainlayer.model.PhoneImageModel
import com.min.scbassignment.databinding.ItemPhoneImageBinding
import com.squareup.picasso.Picasso

class PhoneImageAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var itemList: ArrayList<PhoneImageModel> = ArrayList()

    fun setItems(items: List<PhoneImageModel>) {
        itemList.clear()
        itemList.addAll(items)
        notifyDataSetChanged()
    }


    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as ItemHolder
        var item = itemList[position]
        holder.bind()
        Picasso.get().load(item.imageUrl).into(holder.itemBinding.itemImageView)

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemBinding = ItemPhoneImageBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        )
        return ItemHolder(itemBinding)
    }

    private inner class ItemHolder internal constructor(var itemBinding: ItemPhoneImageBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind() {
            itemBinding.executePendingBindings()
        }
    }
}