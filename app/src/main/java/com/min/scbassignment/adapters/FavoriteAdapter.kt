package com.min.scbassignment.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.min.domainlayer.model.PhoneModel
import com.min.scbassignment.R
import com.min.scbassignment.adapters.interfaces.FavoriteOnSwipe
import com.min.scbassignment.databinding.ItemFavoriteBinding
import com.squareup.picasso.Picasso


class FavoriteAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var itemList: ArrayList<PhoneModel> = ArrayList()
    private lateinit var clickInterface: FavoriteOnSwipe

    fun setInterface(clickInterface: FavoriteOnSwipe) {
        this.clickInterface = clickInterface
    }

    fun setItems(items: List<PhoneModel>) {
        itemList.clear()
        itemList.addAll(items)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        var context = viewHolder.itemView.context
        val holder = viewHolder as ItemHolder
        var item = itemList.get(position)
        holder.bind()
        holder.itemBinding.nameTextView.text = item.name
        holder.itemBinding.descTextView.text = item.description
        holder.itemBinding.priceTextView.text = context.getString(R.string.label_price, item.price.toString())
        holder.itemBinding.ratingTextView.text = context.getString(R.string.label_rating, item.rating.toString())
        Picasso.get().load(item.thumbImageURL).into(holder.itemBinding.phoneImageView)
        holder.itemBinding.root.setOnClickListener {
            clickInterface.onClick(item)
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemBinding = ItemFavoriteBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        )
        return ItemHolder(itemBinding)
    }

    private inner class ItemHolder internal constructor(var itemBinding: ItemFavoriteBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind() {
            itemBinding.executePendingBindings()
        }
    }
}