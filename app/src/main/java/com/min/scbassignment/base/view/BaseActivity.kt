package com.min.scbassignment.base.view

import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.databinding.Observable
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import com.min.scbassignment.base.viewModel.BaseActivityViewModel
import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity(), BaseActivityInterface {

    private var mViewBinding: ViewDataBinding? = null

    protected abstract fun getLayoutResId(): Int
    protected abstract fun getBaseViewModel(): BaseActivityViewModel
    protected abstract fun getLoadingProgressBar(): ProgressBar

    open fun initViewModel() {

        getBaseViewModel().errorMsg.observe(this, Observer {
            it?.let {
                showErrorMsg(it)
            }
        })
    }

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    protected fun <C> getViewBinding(componentType: Class<C>): C {
        return componentType.cast(mViewBinding)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewBinding = DataBindingUtil.setContentView(this, getLayoutResId())
        initViewModel()
    }

    override fun showErrorMsg(text: String) {
        if (!TextUtils.isEmpty(text)) {
            val snackbar = Snackbar.make(getRootView(), text, Snackbar.LENGTH_LONG)
            snackbar.view.setBackgroundColor(ContextCompat.getColor(this, android.R.color.holo_red_light))

            val textView = snackbar.view.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
            textView.maxLines = 5  //this allows snackbar to have maximum 5 lines

            snackbar.show()
        }
    }
}