package com.min.scbassignment.base.view

import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.min.domainlayer.model.PhoneModel
import com.min.scbassignment.base.viewModel.BaseFragmentViewMode
import com.min.scbassignment.enums.SortType
import dagger.android.support.DaggerFragment

abstract class BaseFragment : DaggerFragment(), BaseFragmentInterface {

    private val TAG = "BaseFragment"

    private var mViewBinding: ViewDataBinding? = null

    protected abstract fun getBaseViewModel(): BaseFragmentViewMode
    protected abstract fun getLayoutResId(): Int
    protected abstract fun showError(eror:String)

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    protected fun <C> getViewBinding(componentType: Class<C>): C {
        return componentType.cast(mViewBinding)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mViewBinding = DataBindingUtil.inflate(inflater, getLayoutResId(), container, false)
        return mViewBinding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
    }

    open fun initViewModel() {
        getBaseViewModel().errorMsg.observe(this, Observer {
            it?.let {
                showError(it)
            }
        })
    }

    override fun sort(sortType: SortType) {
        getBaseViewModel().sortPhone(sortType)
    }

    override fun populateList(phoneList: List<PhoneModel>) {
        getBaseViewModel().setData(phoneList)
    }

}