package com.min.scbassignment.base.view

import android.support.v4.app.Fragment
import android.view.View
import android.widget.FrameLayout

interface BaseActivityInterface{
    /**
     * show Error in any way the inherited class want
     */
    fun showErrorMsg(text: String)

    /**
     * the inherited class will provide this view
     */
    fun getFragmentContainer(): FrameLayout?

    /**
     * the inherited class will provide this view
     */
    fun getRootView(): View
}