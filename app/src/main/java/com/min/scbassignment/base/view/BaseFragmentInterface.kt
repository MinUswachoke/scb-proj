package com.min.scbassignment.base.view

import com.min.domainlayer.model.PhoneModel
import com.min.scbassignment.enums.SortType

interface BaseFragmentInterface {
    fun sort(sortType: SortType)
    fun populateList(phoneList: List<PhoneModel>)
}