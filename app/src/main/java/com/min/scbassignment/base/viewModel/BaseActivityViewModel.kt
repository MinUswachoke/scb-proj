package com.min.scbassignment.base.viewModel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableBoolean
import com.min.scbassignment.events.SingleLiveEvent

abstract class BaseActivityViewModel(context: Application): AndroidViewModel(context){
    val isLoading = ObservableBoolean(false)

    val errorMsg = SingleLiveEvent<String>()
}