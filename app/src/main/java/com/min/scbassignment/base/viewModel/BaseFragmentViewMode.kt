package com.min.scbassignment.base.viewModel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableField
import com.min.domainlayer.model.PhoneModel
import com.min.scbassignment.enums.SortType
import com.min.scbassignment.events.SingleLiveEvent

abstract class BaseFragmentViewMode(context:Application): AndroidViewModel(context){
    val phoneList = ObservableField<List<PhoneModel>>()
    val reloadList = SingleLiveEvent<Void>()
    val favoriteReload = SingleLiveEvent<Void>()
    val errorMsg = SingleLiveEvent<String>()

    fun setData(arg: List<PhoneModel>) {
        this.phoneList.set(arg)
    }
    fun sortPhone(sortType: SortType){
        phoneList.get()?.let {
            val tempPhoneList = when(sortType){
                SortType.SORT_PRICE_LOW_TO_HIGH -> sortPriceLowToHigh(it)
                SortType.SORT_PRICE_HIGH_TO_LOW -> sortPriceHighToLow(it)
                SortType.SORT_RATING_HIGH_LOW -> sortRating(it)
            }
            phoneList.set(tempPhoneList)
        }
    }

    fun setFavorite(phoneModel: PhoneModel){
        var tempList = phoneList.get()
        tempList?.let {
            for( i in 0 until it.size){
                if(it[i].id == phoneModel.id){
                    tempList[i].isFav = true
                }
            }
        }
        phoneList.set(tempList)
        reloadList.call()
        favoriteReload.call()
    }

    private fun sortPriceLowToHigh(arg: List<PhoneModel>):List<PhoneModel>{
        return arg.sortedBy {
            it.price
        }
    }

    private fun sortPriceHighToLow(arg: List<PhoneModel>):List<PhoneModel>{
        return arg.sortedByDescending {
            it.price
        }
    }

    private fun sortRating(arg: List<PhoneModel>):List<PhoneModel>{
        return arg.sortedByDescending {
            it.rating
        }
    }


}