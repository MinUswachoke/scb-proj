package com.min.scbassignment.di.view.fragments

import com.min.domainlayer.usecase.favorite.RemoveFavoriteUseCase
import com.min.scbassignment.adapters.FavoriteAdapter
import com.min.scbassignment.applications.MyApplication
import com.min.scbassignment.favorite.FavoriteFragment
import com.min.scbassignment.favorite.FavoriteViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class FavoriteFragmentModule {
    @Binds
    abstract fun provideContext(favoriteFragment: FavoriteFragment): FavoriteFragment


    @Module
    companion object {
        @JvmStatic
        @Provides
        fun provideViewModel(app: MyApplication, removeFavoriteUseCase: RemoveFavoriteUseCase): FavoriteViewModel {
            return FavoriteViewModel(app, removeFavoriteUseCase)
        }

        @JvmStatic
        @Provides
        fun provideAdapter(): FavoriteAdapter {
            return FavoriteAdapter()
        }
    }
}