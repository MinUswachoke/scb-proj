package com.min.scbassignment.di.module

import com.min.scbassignment.di.view.fragments.FavoriteFragmentModule
import com.min.scbassignment.di.view.fragments.ListFragmentModule
import com.min.scbassignment.favorite.FavoriteFragment
import com.min.scbassignment.listpage.ListPageFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {
    @ContributesAndroidInjector(modules = [ListFragmentModule::class])
    abstract fun listFragment(): ListPageFragment

    @ContributesAndroidInjector(modules = [FavoriteFragmentModule::class])
    abstract fun favFragment(): FavoriteFragment
}