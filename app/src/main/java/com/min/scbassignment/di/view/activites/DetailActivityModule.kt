package com.min.scbassignment.di.view.activites

import com.min.domainlayer.usecase.phoneImage.GetPhoneImageUseCase
import com.min.scbassignment.adapters.PhoneImageAdapter
import com.min.scbassignment.applications.MyApplication
import com.min.scbassignment.detailPage.DetailActivity
import com.min.scbassignment.detailPage.DetailViewMode
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class DetailActivityModule {
    @Binds
    abstract fun provideContext(activity: DetailActivity): DetailActivity

    @Module
    companion object {
        @JvmStatic
        @Provides
        fun provideViewModel(app: MyApplication, getPhoneImageUseCase: GetPhoneImageUseCase): DetailViewMode {
            return DetailViewMode(app, getPhoneImageUseCase)
        }


        @JvmStatic
        @Provides
        fun provideAdapter(): PhoneImageAdapter {
            return PhoneImageAdapter()
        }
    }

}