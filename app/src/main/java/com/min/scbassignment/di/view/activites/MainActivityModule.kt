package com.min.scbassignment.di.view.activites

import com.min.domainlayer.usecase.favorite.GetFavoriteListUseCase
import com.min.domainlayer.usecase.phoneList.GetPhoneListUseCase
import com.min.scbassignment.adapters.MainViewAdapter
import com.min.scbassignment.applications.MyApplication
import com.min.scbassignment.mainpage.MainActivity
import com.min.scbassignment.mainpage.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class MainActivityModule{
    @Binds
     abstract fun provideContext(activity: MainActivity): MainActivity

    @Module
       companion object {
        @JvmStatic @Provides fun provideViewModel(app: MyApplication,
                                                  getPhoneListUseCase: GetPhoneListUseCase,
                                                  getFavoriteListUseCase: GetFavoriteListUseCase): MainViewModel {
            return MainViewModel(app,getPhoneListUseCase,getFavoriteListUseCase)
        }

        @JvmStatic @Provides fun provideAdapter(): MainViewAdapter {
            return MainViewAdapter()
        }
    }

}