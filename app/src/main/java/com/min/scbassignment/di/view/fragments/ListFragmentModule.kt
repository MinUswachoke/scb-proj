package com.min.scbassignment.di.view.fragments

import com.min.domainlayer.usecase.favorite.SetFavoriteUseCase
import com.min.scbassignment.adapters.PhoneListAdapter
import com.min.scbassignment.applications.MyApplication
import com.min.scbassignment.listpage.ListPageFragment
import com.min.scbassignment.listpage.ListPageViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class ListFragmentModule {
    @Binds
    abstract fun provideContext(listPageFragment: ListPageFragment): ListPageFragment

    @Module
    companion object {
        @JvmStatic
        @Provides
        fun provideViewModel(
            app: MyApplication, setFavoriteUseCase: SetFavoriteUseCase
        ): ListPageViewModel {
            return ListPageViewModel(app, setFavoriteUseCase)
        }

        @JvmStatic
        @Provides
        fun provideAdapter(): PhoneListAdapter {
            return PhoneListAdapter()
        }
    }
}