package com.min.scbassignment.di.components

import com.min.scbassignment.applications.MyApplication
import com.min.scbassignment.di.module.ActivityBuilderModule
import com.min.scbassignment.di.module.AppModule
import com.min.scbassignment.di.module.FragmentBuilderModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(modules = [AppModule::class, AndroidSupportInjectionModule::class, ActivityBuilderModule::class, FragmentBuilderModule::class])
@Singleton
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: MyApplication): Builder

        fun build(): AppComponent
    }

    fun inject(app: MyApplication)
}