package com.min.scbassignment.di.module

import android.app.Application
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.min.datalayer.network.RetrofitInterface
import com.min.domainlayer.repositories.PhoneRepository
import com.min.datalayer.dataRepositories.PhoneDataRepository
import com.min.datalayer.dataRepositories.PhoneImageDataRepository
import com.min.datalayer.database.AppDatabase
import com.min.datalayer.executors.JobExecutor
import com.min.datalayer.mappers.PhoneEntityDataMapper
import com.min.datalayer.mappers.PhoneImageMapper
import com.min.domainlayer.executors.PostExecutionThread
import com.min.domainlayer.executors.ThreadExecutor
import com.min.domainlayer.repositories.PhoneImageRepository
import com.min.domainlayer.usecase.phoneImage.GetPhoneImageUseCase
import com.min.domainlayer.usecase.phoneList.GetPhoneListUseCase
import com.min.scbassignment.applications.MyApplication
import com.min.scbassignment.threads.UIThread
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import android.arch.persistence.room.Room
import com.min.datalayer.dao.PhoneDao
import com.min.domainlayer.usecase.favorite.GetFavoriteListUseCase
import com.min.domainlayer.usecase.favorite.RemoveFavoriteUseCase
import com.min.domainlayer.usecase.favorite.SetFavoriteUseCase


@Module
class AppModule{
    private val TIMEOUT = 30

    @Provides
    @Singleton
    fun provideApplicationContext(app:MyApplication):Application{
        return app
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        return GsonBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm:ssZ")
            .create()
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val mOkHttpBuilder = OkHttpClient.Builder()
        mOkHttpBuilder.writeTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS)
        mOkHttpBuilder.readTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS)
        mOkHttpBuilder.connectTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS)


        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        mOkHttpBuilder.addInterceptor(logging)

        mOkHttpBuilder.addInterceptor { chain -> chain.proceed(chain.request()) }
        return mOkHttpBuilder.build()
    }

    @Provides
    @Singleton
    internal fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .baseUrl("https://scb-test-mobile.herokuapp.com/api/")
            .build()
    }

    @Provides
    @Singleton
    internal fun provideService(retrofit: Retrofit): RetrofitInterface {
        return retrofit.create<RetrofitInterface>(RetrofitInterface::class.java!!)
    }

    @Provides
    @Singleton
    internal fun providePhoneRepository(retrofitInterface: RetrofitInterface, phoneEntityDataMapper: PhoneEntityDataMapper,phoneDao: PhoneDao): PhoneRepository {
        return  PhoneDataRepository(retrofitInterface,phoneEntityDataMapper,phoneDao)
    }

    @Provides
    @Singleton
    internal fun providePhoneMapper(): PhoneEntityDataMapper {
        return  PhoneEntityDataMapper()
    }

    @Provides
    @Singleton
    internal fun provideGetPhoneListUsecase(phoneRepository: PhoneRepository,threadExecutor: ThreadExecutor,
                                            postExecutionThread: PostExecutionThread): GetPhoneListUseCase {
        return  GetPhoneListUseCase(phoneRepository,threadExecutor,postExecutionThread)
    }

    @Provides
    @Singleton
    internal fun providePostExecutionThread(): PostExecutionThread {
        return UIThread()
    }

    @Provides
    @Singleton
    internal fun provideThreadExecutor(): ThreadExecutor {
        return JobExecutor()
    }

    @Provides
    @Singleton
    internal fun providePhoneImageRepo(retrofitInterface: RetrofitInterface, mapper: PhoneImageMapper): PhoneImageRepository {
        return  PhoneImageDataRepository(retrofitInterface,mapper)
    }

    @Provides
    @Singleton
    internal fun providePhoneImageMapper(): PhoneImageMapper {
        return  PhoneImageMapper()
    }

    @Provides
    @Singleton
    internal fun provideGetPhoneImageUseCase(repository: PhoneImageRepository,threadExecutor: ThreadExecutor,
                                            postExecutionThread: PostExecutionThread): GetPhoneImageUseCase {
        return  GetPhoneImageUseCase(repository,threadExecutor,postExecutionThread)
    }

    @Provides
    @Singleton
    internal fun provideRoomDataBase(app:MyApplication): AppDatabase {
        return Room.databaseBuilder(app, AppDatabase::class.java, "my-db").allowMainThreadQueries().build()
    }

    @Singleton
    @Provides
    fun providesProductDao(database: AppDatabase): PhoneDao {
        return database.phoneDao()
    }

    @Provides
    @Singleton
    internal fun provideSetFavoriteUseCase(repository: PhoneRepository,threadExecutor: ThreadExecutor,
                                             postExecutionThread: PostExecutionThread): SetFavoriteUseCase {
        return  SetFavoriteUseCase(repository,threadExecutor,postExecutionThread)
    }

    @Provides
    @Singleton
    internal fun provideGetFavoriteListUseCase(repository: PhoneRepository,threadExecutor: ThreadExecutor,
                                           postExecutionThread: PostExecutionThread): GetFavoriteListUseCase {
        return  GetFavoriteListUseCase(repository,threadExecutor,postExecutionThread)
    }

    @Provides
    @Singleton
    internal fun provideRemoveFavoriteUseCase(repository: PhoneRepository,threadExecutor: ThreadExecutor,
                                               postExecutionThread: PostExecutionThread): RemoveFavoriteUseCase {
        return  RemoveFavoriteUseCase(repository,threadExecutor,postExecutionThread)
    }



}