package com.min.scbassignment.di.module

import com.min.scbassignment.detailPage.DetailActivity
import com.min.scbassignment.di.view.activites.DetailActivityModule
import com.min.scbassignment.di.view.activites.MainActivityModule
import com.min.scbassignment.mainpage.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun mainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [DetailActivityModule::class])
    abstract fun detailActivity(): DetailActivity
}