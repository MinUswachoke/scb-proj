package com.min.scbassignment.listpage

import com.min.scbassignment.base.view.BaseFragmentInterface
import com.min.scbassignment.mainpage.MainActivityInterface

interface ListPageInterface : BaseFragmentInterface {
    fun setInterface(mainActivityInterface: MainActivityInterface)
    fun removeFavoriteIndex(index: Int)

}