package com.min.scbassignment.listpage

import android.arch.lifecycle.Observer
import android.databinding.Observable
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.min.domainlayer.model.PhoneModel
import com.min.scbassignment.R
import com.min.scbassignment.adapters.PhoneListAdapter
import com.min.scbassignment.adapters.interfaces.MainListOnClick
import com.min.scbassignment.base.view.BaseActivityInterface
import com.min.scbassignment.base.view.BaseFragment
import com.min.scbassignment.base.viewModel.BaseFragmentViewMode
import com.min.scbassignment.databinding.FragmentListBinding
import com.min.scbassignment.mainpage.MainActivityInterface
import javax.inject.Inject

class ListPageFragment: BaseFragment(),ListPageInterface{

    @Inject
    lateinit var viewModel: ListPageViewModel
    @Inject
    lateinit var adapter: PhoneListAdapter

    private lateinit var mViewBinding: FragmentListBinding
    private lateinit var activityInterface: MainActivityInterface

    override fun setInterface(mainActivityInterface: MainActivityInterface) {
        activityInterface = mainActivityInterface
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewBinding = getViewBinding(FragmentListBinding::class.java)
        initializeView()
    }

    override fun initViewModel() {
        super.initViewModel()
        viewModel.phoneList.addOnPropertyChangedCallback(object :Observable.OnPropertyChangedCallback(){
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                viewModel.phoneList.get()?.let {
                    adapter.setItems(it)
                }
            }

        })

        viewModel.reloadList.observe(this, Observer {
            viewModel.phoneList.get()?.let {
                adapter.setItems(it)
            }
        })

        viewModel.favoriteReload.observe(this, Observer {
            activityInterface.favoriteReload()
        })
    }

    override fun getLayoutResId(): Int {
        return R.layout.fragment_list
    }

    override fun getBaseViewModel(): BaseFragmentViewMode {
        return viewModel
    }

    override fun showError(eror: String) {
        (activityInterface as BaseActivityInterface).showErrorMsg(eror)
    }

    override fun removeFavoriteIndex(id: Int) {
        viewModel.removeFavoriteId(id)
    }

    private fun initializeView(){
        mViewBinding.phoneRecyclerView
        mViewBinding.phoneRecyclerView.layoutManager = LinearLayoutManager(activity)
        mViewBinding.phoneRecyclerView.adapter = adapter
        adapter.setInterface(object: MainListOnClick{
            override fun onClickFav(phoneModel: PhoneModel) {
                viewModel.addToFavorite(phoneModel)
            }

            override fun onClick(phoneModel: PhoneModel) {
                activityInterface.showDetailPage(phoneModel)
            }

        })

    }
}