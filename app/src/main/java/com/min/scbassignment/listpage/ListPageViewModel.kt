package com.min.scbassignment.listpage

import android.app.Application
import android.databinding.ObservableField
import android.util.Log
import com.min.domainlayer.model.PhoneModel
import com.min.domainlayer.usecase.favorite.GetFavoriteListUseCase
import com.min.domainlayer.usecase.favorite.SetFavoriteUseCase
import com.min.scbassignment.base.viewModel.BaseFragmentViewMode
import io.reactivex.observers.DisposableObserver

class ListPageViewModel constructor(val app: Application, private val setFavoriteUseCase: SetFavoriteUseCase) : BaseFragmentViewMode(app) {
    private val TAG = "MainViewModel"

    fun addToFavorite(phoneModel: PhoneModel){
        setFavoriteUseCase.execute(object : DisposableObserver<Int>() {
            override fun onComplete() {
                setFavorite(phoneModel)
            }

            override fun onNext(t: Int) {

            }

            override fun onError(e: Throwable) {
                errorMsg.value = e.localizedMessage

            }

        }, phoneModel)
    }

    fun removeFavoriteId(id:Int){
        phoneList.get()?.let {
            val tempList = ArrayList<PhoneModel>()
            tempList.addAll(it)
            var index = 0
            for(i in 0 until tempList.size){
                if(tempList[i].id == id){
                    index = i
                    break
                }
            }

            var tempData = tempList[index]
            tempData.isFav = false
            tempList[index] = tempData
            phoneList.set(tempList)
            reloadList.call()
        }
    }


}