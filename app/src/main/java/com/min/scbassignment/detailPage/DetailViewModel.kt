package com.min.scbassignment.detailPage

import android.app.Application
import android.databinding.ObservableField
import com.min.domainlayer.model.PhoneImageModel
import com.min.domainlayer.usecase.phoneImage.GetPhoneImageUseCase
import com.min.scbassignment.R
import com.min.scbassignment.base.viewModel.BaseActivityViewModel
import io.reactivex.observers.DisposableObserver

class DetailViewMode constructor(val context: Application, private val getPhoneImageUseCase: GetPhoneImageUseCase) :
    BaseActivityViewModel(context) {

    val phoneModelId = ObservableField<Int>()
    val phoneModelName = ObservableField<String>()
    val phoneModelDesc = ObservableField<String>()
    val phoneModelRating = ObservableField<String>()
    val phoneModelPrice = ObservableField<String>()
    val phoneImageList = ObservableField<List<PhoneImageModel>>()
    val phoneModelBrand = ObservableField<String>()

    fun setPhoneModel(id: Int, name: String, desc: String, rating: String, price: String, brand: String) {
        phoneModelId.set(id)
        phoneModelName.set(name)
        phoneModelDesc.set(desc)
        phoneModelRating.set(context.getString(R.string.label_rating, rating))
        phoneModelPrice.set(context.getString(R.string.label_price, price))
        phoneModelBrand.set(brand)
    }

    fun loadImage() {
        phoneModelId.get()?.let {
            isLoading.set(true)
            getPhoneImageUseCase.execute(object : DisposableObserver<List<PhoneImageModel>>() {
                override fun onComplete() {
                    isLoading.set(false)
                }

                override fun onNext(t: List<PhoneImageModel>) {
                    phoneImageList.set(t)
                }

                override fun onError(e: Throwable) {
                    isLoading.set(false)
                    errorMsg.value = e.localizedMessage
                }

            }, it)
        }

    }
}