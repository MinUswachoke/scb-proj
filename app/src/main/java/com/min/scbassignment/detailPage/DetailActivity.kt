package com.min.scbassignment.detailPage

import android.content.Intent
import android.databinding.Observable
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import android.widget.FrameLayout
import android.widget.ProgressBar
import com.min.domainlayer.model.PhoneModel
import com.min.scbassignment.R
import com.min.scbassignment.adapters.PhoneImageAdapter
import com.min.scbassignment.base.view.BaseActivity
import com.min.scbassignment.base.viewModel.BaseActivityViewModel
import com.min.scbassignment.databinding.ActivityDetailBinding
import javax.inject.Inject


class DetailActivity : BaseActivity() {
    @Inject
    lateinit var viewModel: DetailViewMode
    @Inject
    lateinit var adapter: PhoneImageAdapter

    private lateinit var mViewBinding: ActivityDetailBinding

    companion object {
        val PRODUCT_ID = "PRODUCT_ID"
        val PRODUCT_NAME = "PRODUCT_NAME"
        val PRODUCT_DESC = "PRODUCT_DESC"
        val PRODUCT_RATE = "PRODUCT_RATE"
        val PRODUCT_PRICE = "PRODUCT_PRICE"
        val PRODUCT_BRAND = "PRODUCT_BRAND"

        fun getInstance(baseActivity: BaseActivity, phoneModel: PhoneModel) {
            if (baseActivity.isFinishing) {
                return
            }
            val intent = Intent(baseActivity, DetailActivity::class.java)
            intent.putExtra(PRODUCT_ID, phoneModel.id)
            intent.putExtra(PRODUCT_NAME, phoneModel.name)
            intent.putExtra(PRODUCT_DESC, phoneModel.description)
            intent.putExtra(PRODUCT_RATE, phoneModel.rating.toString())
            intent.putExtra(PRODUCT_PRICE, phoneModel.price.toString())
            intent.putExtra(PRODUCT_BRAND, phoneModel.brand)
            baseActivity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewBinding = getViewBinding(ActivityDetailBinding::class.java)
        mViewBinding.viewModel = viewModel
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        if (intent.hasExtra(PRODUCT_ID) && intent.hasExtra(PRODUCT_NAME) && intent.hasExtra(PRODUCT_DESC) &&
            intent.hasExtra(PRODUCT_RATE) && intent.hasExtra(PRODUCT_PRICE) && intent.hasExtra(PRODUCT_BRAND)
        ) {
            viewModel.setPhoneModel(
                intent.getIntExtra(PRODUCT_ID, 0),
                intent.getStringExtra(PRODUCT_NAME),
                intent.getStringExtra(PRODUCT_DESC),
                intent.getStringExtra(PRODUCT_RATE),
                intent.getStringExtra(PRODUCT_PRICE),
                intent.getStringExtra(PRODUCT_BRAND)
            )
        } else {
            finish()
        }
        initAdapter()
        viewModel.loadImage()
    }

    private fun initAdapter() {
        mViewBinding.phoneImageRecyclerView
        mViewBinding.phoneImageRecyclerView.layoutManager =
                LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        mViewBinding.phoneImageRecyclerView.adapter = adapter
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        return true
    }

    override fun initViewModel() {
        viewModel.phoneImageList.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                viewModel.phoneImageList.get()?.let {
                    adapter.setItems(it)
                }

            }
        })
    }

    override fun getBaseViewModel(): BaseActivityViewModel {
        return viewModel
    }

    override fun getLayoutResId(): Int {
        return R.layout.activity_detail
    }

    override fun getLoadingProgressBar(): ProgressBar {
        return mViewBinding.progressbar
    }


    override fun getFragmentContainer(): FrameLayout? {
        return null
    }

    override fun getRootView(): View {
        return mViewBinding.root
    }
}